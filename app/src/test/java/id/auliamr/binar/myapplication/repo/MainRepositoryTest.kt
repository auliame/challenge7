package id.auliamr.binar.myapplication.repo

import id.auliamr.binar.myapplication.Api.ApiHelper
import id.auliamr.binar.myapplication.Api.ApiService
import id.auliamr.binar.myapplication.Database.GetPopularMovieResponse
import id.auliamr.binar.myapplication.Database.PostUserResponse
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MainRepositoryTest {

    private lateinit var service : ApiService
    private lateinit var apiHelper: ApiHelper
    private lateinit var repository: MainRepository

    private val detail = 634649
    private val email = "aulia@gmail.com"
    private val user = PostUserResponse("a", "b", "c", "d", "e", "f")

    @Before
    fun setUp(){
        service = mockk()
        apiHelper = ApiHelper(service, service)
        repository = MainRepository(apiHelper, favoriteDao)
    }

    @Test
    fun getPopularMovie() : Unit = runBlocking {
        val response = mockk<GetPopularMovieResponse>()
        every {
            runBlocking {
                service.getPopularMovie(API_KEY)
            }
        } returns response

        repository.getPopularMovie(API_KEY)

        verify {
            runBlocking {
                service.getPopularMovie(API_KEY)
            }
        }
    }

    @org.junit.Test
    fun getDetailMovie() {
    }

    @org.junit.Test
    fun getUser() {
    }

    @org.junit.Test
    fun addUsers() {
    }

    @org.junit.Test
    fun updateUser() {
    }

    companion object {
        private const val API_KEY = "471e33d18cca1c216a5735f21308be7d"
    }
}