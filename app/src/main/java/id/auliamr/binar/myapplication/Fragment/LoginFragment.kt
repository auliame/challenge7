package id.auliamr.binar.myapplication.Fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import id.auliamr.binar.myapplication.Database.AppDatabase
import id.auliamr.binar.myapplication.R
import id.auliamr.binar.myapplication.databinding.FragmentLoginBinding
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.*
import java.util.regex.Pattern

class LoginFragment : Fragment() , View.OnClickListener {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private var appDatabase : AppDatabase? = null
    private lateinit var sharedPreferences: SharedPreferences

    private lateinit var email: String
    private lateinit var password: String
    private var viewPass : Boolean = false
    private var cek: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = requireActivity().getSharedPreferences(HomeFragment.PREF_USER, Context.MODE_PRIVATE)
        appDatabase = AppDatabase.getInstance(requireContext())

        binding.btnLogin.setOnClickListener(this)
        binding.btnRegister.setOnClickListener(this)
        binding.btnViewPass.setOnClickListener(this)

        if (sharedPreferences.contains(HomeFragment.EMAIL)){
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
        }
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.btn_login ->{
                login()
            }
            R.id.btn_register -> {
                view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
            R.id.btn_view_pass -> {
                if (viewPass == false){
                    binding.apply {
                        btn_view_pass.setImageResource(R.drawable.ic_remove_eye)
                        et_pass.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    }
                    viewPass = true
                }else{
                    binding.apply {
                        btn_view_pass.setImageResource(R.drawable.ic_stroke_eye)
                        et_pass.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                    viewPass = false
                }
            }
        }
    }

    private fun login() {
        binding.apply {
            email = et_email.text.toString()
            password = et_pass.text.toString()
            cek = isValidEmail(email)
        }

        if (inputCheck(email,password,cek)){
            loginUser(email, password)
        }
    }

    private fun inputCheck(email: String, password: String, cek: Boolean) : Boolean{
        if (email.isEmpty() || password.isEmpty() || !cek
        ) {
            if (email.isEmpty()) {
                binding.apply {
                    et_email.setError("Email harus diisi!")
                    et_email.requestFocus()
                }

            }
            if (password.isEmpty()) {
                binding.apply {
                    et_pass.setError("Password harus diisi!")
                    et_pass.requestFocus()
                }
            }
            if (!cek) {
                binding.apply {
                    et_email.setError("Format email tidak sesuai!")
                    et_email.requestFocus()
                }
            }
            return false
        }else{
            return true
        }
    }

    private fun loginUser(email : String, password : String) {
        GlobalScope.async {
            val user = appDatabase?.UserDao()?.getUserRegistered(email)
            requireActivity().runOnUiThread{
                if (user != null) {
                    if (email == user.email && password == user.password){
                        val editor = sharedPreferences.edit()
                        editor.putString(HomeFragment.EMAIL, email)
                        editor.apply()
                        Navigation.findNavController(requireView()).navigate(R.id.action_loginFragment_to_homeFragment)
                    }else{
                        Toast.makeText(requireContext(), "Password yang anda masukkan salah", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(requireContext(), "Akun dengan email ${email} belum terdaftar", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern = Pattern.compile(
            "[a-zA-Z0-9+._%\\-]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )
        return emailPattern.matcher(email).matches()
    }

    companion object {
        const val PREF_USER = "user_preference"
        const val EMAIL = "email"
    }

}