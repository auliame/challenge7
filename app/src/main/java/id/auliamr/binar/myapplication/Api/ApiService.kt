package id.auliamr.binar.myapplication.Api

import id.auliamr.binar.myapplication.Database.GetDetailMovieResponse
import id.auliamr.binar.myapplication.Database.GetPopularMovieResponse
import id.auliamr.binar.myapplication.Database.GetUserResponseItem
import id.auliamr.binar.myapplication.Database.PostUserResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @GET("/3/movie/popular")
    suspend fun getPopularMovie(
        @Query("api_key") api_key: String
    ): GetPopularMovieResponse

    @GET("/3/movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") api_key: String
    ): GetDetailMovieResponse


    @GET("/3/movie/{movie_id}/similar")
    suspend fun getSimilarMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") api_key: String
    ): GetPopularMovieResponse

    //=====================User========================

    @GET("/users")
    suspend fun getUser(
        @Query("email") email: String
    ) : List<GetUserResponseItem>

    @POST("/users")
    suspend fun addUsers(
        @Body user : PostUserResponse
    ): GetUserResponseItem

    @PUT("/users/{id}")
    suspend fun updateUser(
        @Body user : PostUserResponse, @Path("id") id:String
    ) : GetUserResponseItem

}