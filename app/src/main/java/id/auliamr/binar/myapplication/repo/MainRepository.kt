package id.auliamr.binar.myapplication.repo

import id.auliamr.binar.myapplication.Api.ApiHelper
import id.auliamr.binar.myapplication.Database.PostUserResponse
import javax.inject.Inject

class MainRepository@Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getPopularMovie(apiKey: String) = apiHelper.getPopularMovies(apiKey)
    suspend fun getDetailMovie(id: Int, apiKey: String) = apiHelper.getDetailMovie(id, apiKey)
    suspend fun getUser(email: String) = apiHelper.getUser(email)
    suspend fun addUsers(user: PostUserResponse) = apiHelper.addUsers(user)
    suspend fun updateUser(user: PostUserResponse, id: String) = apiHelper.updateUser(user, id)

}