package id.auliamr.binar.myapplication.Api

import id.auliamr.binar.myapplication.Database.PostUserResponse
import id.auliamr.binar.myapplication.Api.ApiService
import javax.inject.Inject
import javax.inject.Named

class ApiHelper @Inject constructor(
    @Named("Movie")
    private val apiServiceMovie: ApiService,
    @Named("User")
    private val apiServiceUser: ApiService
) {

    suspend fun getPopularMovies(apiKey: String) = apiServiceMovie.getPopularMovie(apiKey)
    suspend fun getDetailMovie(id: Int, apiKey: String) = apiServiceMovie.getDetailMovie(id, apiKey)

    suspend fun getUser(email:String) = apiServiceUser.getUser(email)
    suspend fun addUsers(user: PostUserResponse) = apiServiceUser.addUsers(user)
    suspend fun updateUser(user: PostUserResponse, id: String) = apiServiceUser.updateUser(user, id)
}