package id.auliamr.binar.myapplication.Database

import com.google.gson.annotations.SerializedName

data class ResultMovie(

    @SerializedName("genre_ids")
    val genreIds: List<Int>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("poster_path")
    val posterPath: String
)